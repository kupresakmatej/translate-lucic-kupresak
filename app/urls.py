from django.urls import path
from . import views

app_name = "app"
urlpatterns = [
    path('new', views.post_job, name="post_job"),
    path('jobs/<int:job_id>', views.job_detail, name="job_detail"),
    path('', views.home, name="home"),
    path('jobs/<int:job_id>/bid', views.bid, name="bid"),
    path('message/<int:job_id>', views.message, name="message"),
    path('login', views.login, name="login"),
    path('bids/<int:bid_id>/accept', views.accept_bid, name="accept_bid"),
    path('register/', views.register, name="register"),
    path('post_job', views.post_job, name="post_job"),
    path('list_jobs', views.list_jobs, name="list_jobs"),
    path('complete_job/<int:job_id>', views.complete_job, name="complete_job"),
    path('complete_job/<int:job_id>/submit_translation', views.submit_translation, name="submit_translation"),
    path('translated_job/<int:job_id>', views.translated_job, name="translated_job"),
    path('dispute/<int:job_id>', views.dispute, name="dispute"),
    path('dispute/<int:job_id>/raise_dispute', views.raise_dispute, name="raise_dispute"),
    path('review/<int:job_id>', views.review, name="review"),
    path('review/<int:job_id>/post_review', views.post_review, name="post_review"),
]
