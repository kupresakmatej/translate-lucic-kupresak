from .models import Job, Bid, Dispute, Message, Rating, Account
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from django import forms
from django.forms import ModelForm


class JobForm(ModelForm):
    class Meta:
        model = Job
        fields = ['title', 'description', 'field',
                  'source_lang', 'target_lang', 'budget', 'text']

        widgets = {
            'text': forms.Textarea(attrs={'rows': 20}),
        }

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email']

        help_texts = {
            'username': None,
            'email': None,
        }
        

    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    #https://docs.djangoproject.com/en/4.1/topics/forms/modelforms/#interaction-with-model-validation
    #https://docs.djangoproject.com/en/4.1/ref/forms/validation/#cleaning-a-specific-field-attribute
    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2
        
class BidForm(forms.Form):
    price = forms.DecimalField(max_digits=8, decimal_places=2, min_value=0.01)
