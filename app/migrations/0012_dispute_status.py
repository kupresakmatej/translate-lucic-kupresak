# Generated by Django 4.1.6 on 2023-02-25 18:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_alter_job_field_alter_job_source_lang_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='dispute',
            name='status',
            field=models.CharField(choices=[('open', 'Open'), ('closed', 'Closed')], default='open', max_length=100),
        ),
    ]
