from django.db import models
from django.contrib.auth.models import User
from django.db.models import Avg

# Create your models here.


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    balance = models.DecimalField(max_digits=8, decimal_places=2)
    translator = models.BooleanField()

    def __str__(self):
        return f"{self.id} - {self.name}"
    
    def get_average_rating(self):
        ratings = Rating.objects.filter(rated=self.user)
        average_rating = ratings.aggregate(Avg('rating'))['rating__avg']
        return round(average_rating, 2) if average_rating else 0


class Job(models.Model):
    class Status(models.TextChoices):
        AVAILABLE = 'available', "Available"
        ASSIGNED = 'assigned', "Assigned"
        IN_PROGRESS = 'in_progress', "In Progress"
        COMPLETED = 'completed', "Completed"

    class Field(models.TextChoices):
        ART = 'art', "Art"
        BUS = 'business', "Business"
        COMP = 'computers', "Computers"
        EDU = 'education', "Education"
        ENG = 'engineering', "Engineering"
        FIN = 'finance', "Finance"
        LAW = 'law', "Law"
        LIT = 'literature', "Literature"
        MED = 'medicine', "Medicine"
        SCI = 'science', "Science"
        SOC = 'social_sciences', "Social Sciences"
        TECH = 'technology', "Technology"

    class Language(models.TextChoices):
        ENG = 'en', "English"
        SPA = 'spa', "Spanish"
        FRE = 'fr', "French"
        GER = 'de', "German"
        ITA = 'it', "Italian"
        JPN = 'ja', "Japanese"
        CRO = 'hr', "Croatian"

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    source_lang = models.CharField(
        max_length=3,
        choices=Language.choices,
        default=Language.ENG,
    )
    target_lang = models.CharField(
        max_length=3,
        choices=Language.choices,
        default=Language.CRO,
    )
    field = models.CharField(
        max_length=100,
        choices=Field.choices,
        default=Field.ENG,
    )
    budget = models.DecimalField(max_digits=8, decimal_places=2)
    text = models.TextField()
    status = models.CharField(
        max_length=100,
        choices=Status.choices,
        default=Status.AVAILABLE,
    )
    translation = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.user.account.name} - {self.title}"

    def accepted_bid(self):
        return self.bid_set.filter(accepted=True).first()

    def accepted_bidder(self):
        return self.accepted_bid().bidder.account
    
    def accepted_bidder_user(self):
        return self.accepted_bid().bidder

    def owner(self):
        return self.user.account

    # Helper method to check if the job is available, since checking the status
    # directly from the template is not very readable.
    def is_available(self):
        return self.status == self.Status.AVAILABLE

    def is_completed(self):
        return self.status == self.Status.COMPLETED
    
    def accept_bid(self, bid):
        bid.accepted = True
        bid.save()
        self.status = self.Status.ASSIGNED
        self.save()

    def has_open_dispute(self):
        return self.dispute_set.filter(status='open').exists()
    
    def has_closed_dispute(self):
        return self.dispute_set.filter(status='closed').exists()

    @classmethod
    def accepted_jobs_for(cls, user):
        # Svi bidovi gdje je user napravio job na koji je biddano
        accepted_bids = Bid.objects.filter(job__in=user.job_set.all(), accepted=True)
        # Svi jobovi gdje je user napravio job i bidano je na job 
        accepted_jobs = [ bid.job for bid in accepted_bids if bid.job.status != "completed" ]
        return accepted_jobs


class Bid(models.Model):
    bidder = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    accepted = models.BooleanField()

    def __str__(self):
        return f"{self.bidder.account.name} - {self.job.title} - {self.price}"
    def job_owner(self):
        return self.job.user


class Dispute(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    reason = models.TextField()

    class Status(models.TextChoices):
        OPEN = 'open', "Open"
        CLOSED = 'closed', "Closed"

    status = models.CharField(
        max_length=100,
        choices=Status.choices,
        default=Status.OPEN,
    )

    def __str__(self):
        return f"{self.job.user.account.name} - {self.job.title} - dispute"


class Message(models.Model):
    sender = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="sender")
    reciever = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="reciever")
    text = models.TextField()
    sent_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.sender.account.name} - {self.reciever.account.name} - {self.text[:20]}"


class Rating(models.Model):
    rater = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="rater")
    rated = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="rated")
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    rating = models.IntegerField()

    def __str__(self):
        return f"{self.job.user.account.name} - {self.job.title} - rating - {self.rating}"
