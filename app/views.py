from decimal import Decimal, InvalidOperation
import sys
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from .models import Job, Message, Account, Bid, Dispute, Rating
from django.contrib.auth import login, authenticate
from django.contrib import messages
from django.shortcuts import  render, redirect
from django.contrib.auth.models import User
from .forms import BidForm

# Create your views here.


def home(request):
    jobs = Job.objects.all()
    context = {
        'job_list': jobs,
    }
    return render(request, 'app/home.html', context)

def login(request, user):
    if request.method == "POST":
        username = request.POST.get('username', '')
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        
        # Try to authenticate with username and password
        user = authenticate(username=username, password=password)
        
        if user is None and username:
            # Try to get user with email and verify password
            try:
                user = User.objects.get(username=username)
                if not user.check_password(password):
                    user = None
            except User.DoesNotExist:
                user = None
        
        if user is not None:
            # Login successful
            login(request, user)
            return HttpResponseRedirect(reverse('app:home'))
        
        # Authentication failed
        message = 'Invalid login credentials'
    else:
        message = None
    
    context = {'message': message}
    return render(request, 'registration/login.html', context)

def register(request):
    if request.method == "POST":
        name = request.POST.get('name')
        translator = request.POST.get('checkbox') == 'on'
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        password1 = request.POST.get('password1')

        if password != password1:
            return render(request, 'registration/register.html', {"message" : "Passwords must match."})

        if User.objects.filter(email=email).exists():
            return render(request, 'registration/register.html', {"message" : "Email already in use."})

        user = User.objects.create(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()

        account = Account.objects.create(
            user=user,
            name=name,
            balance=0,
            translator=translator,
        )

        return HttpResponseRedirect(reverse('app:login'))

    context = {}
    return render(request, 'registration/register.html', context)

@login_required
def post_job(request):
    if request.method == "POST":
        job = Job.objects.create(
            user=request.user,
            title=request.POST['title'],
            description=request.POST['description'],
            source_lang=request.POST['source_lang'],
            target_lang=request.POST['target_lang'],
            field=request.POST['field'],
            budget=request.POST['budget'],
            text=request.POST['text'],
        )
        return HttpResponseRedirect(reverse('app:home', args=[]))

    context = {}
    return render(request, 'app/post_job.html', context)


@login_required
def message(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    if request.method == "POST":
        message = Message.objects.create(
            sender=request.user,
            reciever=job.user,
            text=request.POST['message_send'],
            sent_at=timezone.now()
        )

        return HttpResponseRedirect(reverse('app:message', args=[job_id]))

    context = {
        'job': job,
    }
    return render(request, 'app/message.html', context)

@login_required
def list_jobs(request):
    job_list = Job.objects.all()
    bids = Bid.objects.filter(bidder=request.user)
    job_ids = [bid.job.id for bid in bids]
    context = {
        'job_list': job_list,
        'bids': bids,
        'bidded_job_ids': job_ids
    }
    return render(request, 'app/list_jobs.html', context)

@login_required
def job_detail(request, job_id):
    job = get_object_or_404(Job, pk=job_id)
    form = BidForm()
    user = request.user
    bid_on = job.id in user.bid_set.all().values_list('job', flat = True)
    context = {
        'job': job,
        'form': form,
        'bid_on' : bid_on,
    }
    return render(request, 'app/jobs_detail.html', context)

@login_required
def bid(request, job_id):
    if request.method == 'POST':
        job = get_object_or_404(Job, pk=job_id)
        price = request.POST.get('price')

        bid = Bid(
            price = price,
            job=job, 
            bidder=request.user,
            accepted = False
        )
        bid.full_clean()
        bid.save()

        return HttpResponseRedirect(reverse('app:job_detail', args=[job_id]))

    context = {
        
    }
    return render(request, 'app/jobs_detail.html', context)

@login_required
def accept_bid(request, bid_id):
    bid = get_object_or_404(Bid, pk=bid_id)
    job = bid.job

    # check if the user is the job owner, otherwise anyone can accept any bid
    if request.method == 'POST' and job.user == request.user:
        bid.accepted = True
        bid.save()
        job.status = Job.Status.ASSIGNED
        job.save()
        return HttpResponseRedirect(reverse('accounts:dashboard'))

    context = {
        
    }
    return render(request, 'accounts/dashboard.html', context)
    
@login_required
def complete_job(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    if request.method == 'POST':
        return HttpResponseRedirect(reverse('app:complete_job', args=[job_id]))

    context = {
        'job': job,
    }
    return render(request, 'app/complete_job.html', context)

@login_required
def submit_translation(request, job_id):
    job = get_object_or_404(Job, id=job_id)

    if request.method == 'POST':
        translation = request.POST.get('text')
        job.translation = translation
        job.status = Job.Status.COMPLETED
        job.save()
        return HttpResponseRedirect(reverse('accounts:profile', args=[request.user.id]))
    
    return render(request, 'app/complete_job.html', {'job': job})

@login_required
def translated_job(request, job_id):
    job = get_object_or_404(Job, id=job_id)
    bid = job.accepted_bid()

    if request.user.id == job.user.id and request.method == 'POST':
        return HttpResponseRedirect(reverse('app:translated_job', args=[job_id]))
    
    context = {
        'job': job,
        'bid': bid,
    }

    return render(request, 'app/translated_job.html', context)

@login_required
def dispute(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    if request.method == 'POST':
        return HttpResponseRedirect(reverse('app:dispute', args=[job_id]))
    
    context = {
        'job': job,
    }

    return render(request, 'app/dispute.html', context)

@login_required
def raise_dispute(request, job_id):
    if request.method == 'POST':
        job = get_object_or_404(Job, pk=job_id)
        user = job.user
        reason = request.POST.get('text')

        dispute = Dispute(
            user = user,
            job = job,
            reason = reason,
        )

        dispute.full_clean()
        dispute.save()

        return HttpResponseRedirect(reverse('accounts:profile', args=[request.user.id]))

    context = {

    }

    return render(request, 'app/dispute.html', context)

@login_required
def review(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    if request.method == 'POST':
        return HttpResponseRedirect(reverse('app:review', args=[job_id]))
    
    context = {
        'job': job,
    }

    return render(request, 'app/review.html', context)

@login_required
def post_review(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    if request.method == 'POST':
        rating_number = request.POST.get('rating')
        rater = request.user
        rated = job.accepted_bidder_user()
        rating = Rating(
            rater = rater,
            rated = rated,
            job = job,
            rating = rating_number,
        )
        rating.save()
        return HttpResponseRedirect(reverse('accounts:dashboard'))

    context = {'job': job}
    return render(request, 'app/post_review.html', context)