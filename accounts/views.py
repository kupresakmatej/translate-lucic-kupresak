from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from app.models import Account, Job, Bid, Dispute, Message, Rating
from django.db.models import Avg


@login_required
def dashboard(request):
    user = request.user
    my_jobs = user.job_set.all()
    my_messages = Message.objects.filter(
        reciever=user) | Message.objects.filter(sender=user)
    my_messages = my_messages.order_by("sent_at")
    context = {
        'my_jobs': my_jobs,
        'my_messages': my_messages,
    }
    return render(request, 'accounts/dashboard.html', context)


def profile(request, user_id):
    user = get_object_or_404(User, pk=user_id)

    completed_jobs = Job.objects.filter(user=user, status="completed")
    accepted_jobs = Job.accepted_jobs_for(user)

    bids = user.bid_set.all()
    won = user.bid_set.filter(accepted=True)

    context = {
        'profile_user': user,
        'accepted_jobs': accepted_jobs,
        'completed_jobs': completed_jobs,
        'bids': bids,
        'won': won,
    }
    return render(request, 'accounts/profile.html', context)

